(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

(setq straight-use-package-by-default t)

(use-package undo-fu)

(use-package general
  :config
  (general-create-definer evil-leader-def
    :prefix "SPC"
    :states 'normal))

(use-package evil
  :custom
  (evil-undo-system 'undo-fu)
  :config
  (setq evil-default-state 'emacs)
  (evil-set-initial-state 'prog-mode 'normal)
  (evil-set-initial-state 'org-mode 'normal)
  (general-def 'motion
              "h" nil
              "j" nil
              "k" nil
              "l" nil
              "<down>" 'evil-next-visual-line
              "<up>" 'evil-previous-visual-line)
  (evil-mode))

(define-key key-translation-map (kbd "ESC") (kbd "C-g"))

(use-package hydra)

(use-package which-key
    :config
    (which-key-mode)
    (setq which-key-idle-delay 1))

(use-package helpful
    :bind
    ([remap describe-variable] . #'helpful-variable)
    ([remap describe-key] . #'helpful-key)
    ([remap describe-function] . #'helpful-callable))

(use-package selectrum
    :bind
    (:map selectrum-minibuffer-map
        ("C-j" . next-line-or-history-element)
        ("C-k" . previous-line-or-history-element))
    :config
    (selectrum-mode 1))

(use-package selectrum-prescient
    :after prescient
    :config
    (selectrum-prescient-mode 1))

(use-package prescient
    :init
    (setq prescient-filter-method '(literal regexp fuzzy))
    (setq prescient-sort-length-enable nil)
    :config
    (prescient-persist-mode 1))

(use-package marginalia
  :bind (:map minibuffer-local-map
         ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode)
  (advice-add #'marginalia-cycle :after
              (lambda () (when (bound-and-true-p selectrum-mode) (selectrum-exhibit))))
  (setq marginalia-annotators '(marginalia-annotators-heavy nil)))

(use-package consult
  :bind
  ("C-s" . consult-line))

(setq compilation-scroll-output t)

(use-package popper
  :ensure t ; or :straight t
  :bind (("C-`"   . popper-toggle-latest)
         ("M-`"   . popper-kill-latest-popup))
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "\\*Warnings\\*"
          "\\*rustic-compilation\\*"
          "Output\\*$"
          helpful-mode
          compilation-mode))
  (popper-mode +1))

(use-package shackle
  :config
  (setq shackle-rules '(("\\*Org Src.*\\*" :regexp t :popup t :align below)
                   ("\\*rustic-compilation\\*" :regexp t :size 0.5 :align below :select t)
                   ("^\\*Python :: Run" :regexp t :size 0.1 :align below)
                   (helpful-mode :popup t :size 0.5 :align below)
                   (compilation-mode :size 0.5 :popup t :align below :select t)))
  (shackle-mode 1))

(setq-default indent-tabs-mode nil)

(defun psi/grep ()
  "Ripgrep in either current or project's directory."
  (interactive)
  (if (projectile-project-p)
      (consult-ripgrep (projectile-acquire-root))
    (consult-ripgrep)))

(general-define-key
 "C-x f" 'psi/grep)

(setq inhibit-startup-message t)

(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room

(menu-bar-mode -1)          ; Disable the menu bar

;; Set up the visible bell
(setq visible-bell t)

(column-number-mode 1)

(hl-line-mode 1)

;; Disable warnings. Gets ugly with nativecomp
(setq warning-minimum-level :emergency)

(set-face-attribute 'default nil :font "Victor Mono Nerd Font 18")
(set-frame-font "Victor Mono Nerd Font 18")

(use-package ligature
  :straight (ligature :type git :host github :repo "mickeynp/ligature.el")
  :config
  (ligature-set-ligatures 't '("==" "!=" "::" "=>" "->"))
  (global-ligature-mode 1))

(use-package doom-modeline
    :init
    (setq doom-modeline-height 40
            doom-modeline-enable-word-count t
            doom-modeline-buffer-encoding nil
            doom-modeline-env-load-string "ΦΦΦ"
            )
    (set-face-attribute 'mode-line nil
                        :family "MesloLGM Nerd Font")
    (doom-modeline-mode 1))

(use-package doom-themes
  :config
  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)

  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config)

  ;; Load both themes without enabling
  (load-theme 'doom-solarized-light t t)
  (load-theme 'doom-solarized-dark t t))

(defun psi/set-theme-dark ()
  (interactive)
  (disable-theme 'doom-solarized-light)
  (enable-theme 'doom-solarized-dark))

(defun psi/set-theme-light ()
  (interactive)
  (disable-theme 'doom-solarized-dark)
  (enable-theme 'doom-solarized-light))

(defun psi/set-theme-auto ()
  (interactive)
  (let ((hour (string-to-number
               (substring (current-time-string) 11 13))))
    (if (member hour (number-sequence 6 13))
        (psi/set-theme-light)
      (psi/set-theme-dark))))

;;(psi/set-theme-auto)
;;(run-at-time "14:00" nil 'psi/set-theme-auto)
;;(run-at-time "6:00" nil 'psi/set-theme-auto)

(psi/set-theme-dark)

(use-package all-the-icons)

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package org
  :config
  (setq org-src-window-setup 'plain)
  (setq org-ellipsis " φ"))

(with-eval-after-load 'org
  (require 'org-tempo)
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("sql" . "src sql"))
  (add-to-list 'org-structure-template-alist '("py" . "src python")))

(use-package org-superstar
  :hook (org-mode . org-superstar-mode)
  :init
  (setq org-superstar-headline-bullets-list '("𝚿" "𝛙" "𝝭" "𝞇")
        org-hide-leading-stars nil
        org-superstar-leading-bullet ?\s
        org-indent-mode-turns-on-hiding-stars nil
        org-superstar-prettify-item-bullets nil
        org-superstar-special-todo-items t))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((sql . t)
   (python . t)
   (haskell . t)
   (emacs-lisp . t)))

(setq org-confirm-babel-evaluate nil)

(setq org-latex-listings 'minted
      org-latex-packages-alist '(("" "minted"))
      org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

(use-package plantuml-mode)
(setq org-plantuml-jar-path "/usr/share/java/plantuml/plantuml.jar")
(add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
(org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t)))

(use-package company
  :after lsp-mode
  :hook (prog-mode . company-mode)
  :bind (:map company-active-map
         ;;("C-j" . company-select-next)
         ;;("C-k" . company-select-previous)
         ("<esc>" . company-abort)
         ("<tab>" . company-complete-selection))
        (:map lsp-mode-map
         ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode)
  :init
    (setq company-box-icons-alist 'company-box-icons-all-the-icons
        company-box-icons-all-the-icons
        (let ((all-the-icons-scale-factor 1)
                (all-the-icons-default-adjust 0))
            `((Unknown       . ,(all-the-icons-faicon "question" :face 'all-the-icons-purple)) ;;question-circle is also good
            (Text          . ,(all-the-icons-faicon "file-text-o" :face 'all-the-icons-green))
            (Method        . ,(all-the-icons-faicon "cube" :face 'all-the-icons-dcyan))
            (Function      . ,(all-the-icons-faicon "cube" :face 'all-the-icons-dcyan))
            (Constructor   . ,(all-the-icons-faicon "cube" :face 'all-the-icons-dcyan))
            (Field         . ,(all-the-icons-faicon "tag" :face 'all-the-icons-red))
            (Variable      . ,(all-the-icons-faicon "tag" :face 'all-the-icons-dpurple))
            (Class         . ,(all-the-icons-faicon "cog" :face 'all-the-icons-red))
            (Interface     . ,(all-the-icons-faicon "cogs" :face 'all-the-icons-red))
            (Module        . ,(all-the-icons-alltheicon "less" :face 'all-the-icons-red))
            (Property      . ,(all-the-icons-faicon "wrench" :face 'all-the-icons-red))
            (Unit          . ,(all-the-icons-faicon "tag" :face 'all-the-icons-red))
            (Value         . ,(all-the-icons-faicon "tag" :face 'all-the-icons-red))
            (Enum          . ,(all-the-icons-faicon "file-text-o" :face 'all-the-icons-red))
            (Keyword       . ,(all-the-icons-material "format_align_center" :face 'all-the-icons-red))
            (Snippet       . ,(all-the-icons-material "content_paste" :face 'all-the-icons-red))
            (Color         . ,(all-the-icons-material "palette" :face 'all-the-icons-red))
            (File          . ,(all-the-icons-faicon "file" :face 'all-the-icons-red))
            (Reference     . ,(all-the-icons-faicon "tag" :face 'all-the-icons-red))
            (Folder        . ,(all-the-icons-faicon "folder" :face 'all-the-icons-red))
            (EnumMember    . ,(all-the-icons-faicon "tag" :face 'all-the-icons-red))
            (Constant      . ,(all-the-icons-faicon "tag" :face 'all-the-icons-red))
            (Struct        . ,(all-the-icons-faicon "cog" :face 'all-the-icons-red))
            (Event         . ,(all-the-icons-faicon "bolt" :face 'all-the-icons-red))
            (Operator      . ,(all-the-icons-faicon "tag" :face 'all-the-icons-red))
            (TypeParameter . ,(all-the-icons-faicon "cog" :face 'all-the-icons-red))
            (Template      . ,(all-the-icons-faicon "bookmark" :face 'all-the-icons-dgreen)))))
  :custom
  (company-box-icons-alist 'company-box-icons-all-the-icons))

(use-package company-prescient
  :init
  (company-prescient-mode))

(use-package lsp-mode
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-c l")
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         ;;(python-mode . lsp)
         ;; if you want which-key integration
         (lsp-mode . lsp-enable-which-key-integration))
  :config
  (evil-leader-def
    :keymaps 'projectile-mode-map
    "g" 'magit)
  :commands lsp)

(use-package lsp-ui
  :commands lsp-ui-mode
  :config
  (evil-leader-def
    :keymaps 'lsp-ui-mode-map
    "D" 'lsp-ui-doc-mode)
  :custom
  (lsp-ui-doc-enable nil)
  (lsp-eldoc-enable-hover t)
  (lsp-signature-auto-activate t)
  (lsp-signature-render-documentation nil)
  :bind
  (:map lsp-ui-peek-mode-map
        ("C-j" . lsp-ui-peek--select-next)
        ("C-k" . lsp-ui-peek--select-prev))
  (:map evil-normal-state-map
        ("g r" . lsp-ui-peek-find-references)))

;;(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

(use-package dap-mode
  :config
  (evil-leader-def
    :keymaps 'dap-mode-map
    "d" 'dap-hydra))

(use-package projectile
  :init
  (projectile-mode 1)
  (push "venv" projectile-globally-ignored-directories)
  (push "*docs" projectile-globally-ignored-directories)
  :custom
  (projectile-project-search-path '("~/Projects"))
  (projectile-indexing-method 'native)
  (projectile-auto-discover nil)
  :bind
  (:map projectile-mode-map
        ("C-c p" . projectile-command-map)))

(use-package flycheck
  :hook (after-init-hook . global-flycheck-mode))

(use-package pyvenv
  :ensure t
  :init
  (setenv "WORKON_HOME" "~/.pyenv/versions"))

(use-package auto-virtualenv)
(add-hook 'python-mode-hook 'auto-virtualenv-set-virtualenv)
(add-hook 'window-configuration-change-hook 'auto-virtualenv-set-virtualenv)
(add-hook 'focus-in-hook 'auto-virtualenv-set-virtualenv)

(use-package lsp-python-ms
  :ensure t
  :init (setq lsp-python-ms-auto-install-server t)
  :hook (python-mode . (lambda ()
                          (require 'lsp-python-ms)
                          (lsp))))  ; or lsp-deferred
;; Python dap
(require 'dap-python)
(push "[/\\\\]\\venv\\'" lsp-file-watch-ignored-directories)

(use-package python-black
    :demand t
    :after python
    :hook (python-mode . python-black-on-save-mode-enable-dwim))

(use-package rustic
  :hook (rustic-cargo-run-mode . comint-mode)
  :config
  (setq lsp-rust-analyzer-cargo-watch-command "clippy"
        lsp-rust-analyzer-server-display-inlay-hints t)
  (evil-leader-def
    :keymaps 'rustic-mode-map
    "c" 'rustic-recompile
    "r" 'rustic-cargo-run
    "f" 'rustic-format-buffer))

(require 'dap-lldb)
(require 'dap-gdb-lldb)
  ;; installs .extension/vscode
  (dap-gdb-lldb-setup)
  (dap-register-debug-template
   "Rust::LLDB Run Configuration"
   (list :type "lldb"
         :request "launch"
         :name "LLDB::Run"
         :gdbpath "rust-lldb"
         :target nil
         :cwd nil))

(use-package exec-path-from-shell
  :ensure
  :init (exec-path-from-shell-initialize))

(use-package emmet-mode)
(use-package company-web)
(defun my-web-mode-hook ()
  (set (make-local-variable 'company-backends) '(company-web-html)))
(use-package impatient-mode)

(defun psi/launch-impatient ()
    (interactive)
    (when 'httpd-running-p
        (httpd-stop))
    (httpd-start)
    (browse-url-xdg-open "http://localhost:8080/imp/"))

(use-package web-mode
  :hook (web-mode . lsp)
  (web-mode . emmet-mode)
  (web-mode . my-web-mode-hook)
  (web-mode . impatient-mode)
  :init
  (evil-leader-def
    :keymaps 'web-mode-map
    "r" 'psi/launch-impatient)
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode)))

(defun psi/refresh-impatient ()
  (interactive)
  (dolist (b (imp--buffer-list))
    (with-current-buffer b (imp--on-change))))

(use-package scss-mode
  :hook
  (scss-mode . lsp)
  (scss-mode . (lambda ()
                 (add-hook 'compilation-finish-functions 'psi/refresh-impatient nil t))))

(use-package haskell-mode)

(add-hook 'c++-mode-hook 'lsp)

(use-package exwm)
(require 'exwm-config)
(exwm-config-default)
